<?php 
  include("template/conf.php");
  $sql = "select * from pages";
  $sql_result= mysqli_query($con,$sql);
?>
<section class="admin_menu_container">  
  <?php include("template/admin_left_menu.php");?>
</section>
<section class="admin_content_container">
  <?php 
    if(isset($_GET["create"]) == "success")
    {
  ?>
  <h5>Congrates... your page is created.</h5>
    <?php } ?>
  <table width="90%">
    <thead>
      <tr>
        <th width="30">SL</th>
        <th width="120">Page Name</th>
        <th width="150">Page Description</th>
        <th width="180">Page Content</th>
        <th width="100">Action</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($sql_result as $key => $value) {
      if($i%2==0){ $bgcolor="#fff";} else {$bgcolor="#E9F3FF";}
      ?>
      <tr bgcolor="<?php echo $bgcolor;?>">
        <td><?php echo $i;?></td>
        <td><?php echo $value["page_name"];?></td>
        <td><?php echo $value["page_description"];?></td>
        <td><?php echo $value["content"];?></td>
        <td>
        <a href="?a=edit">Edit </a> | <a href="?a=delete">Delete</a>
        </td>
      </tr>
      <?php
      $i++;
    }
    ?>
    </tbody>
  </table>
  
</section>